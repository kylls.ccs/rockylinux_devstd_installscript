# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

POWERLINE_HOME=$(python3 -c "
import pkg_resources

try:
    dist = pkg_resources.get_distribution('powerline-status')
    print(dist.location)
except pkg_resources.DistributionNotFound:
    raise SystemExit(1)
")
if [ -d "$POWERLINE_HOME" ]
then
    source "$POWERLINE_HOME/powerline/bindings/bash/powerline.sh"
    export POWERLINE_HOME
fi
