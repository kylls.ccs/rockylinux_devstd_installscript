#!/bin/bash
# 1. copy the script to $HOME
\cp -f installscript.sh $HOME/installscript.sh
\cp -f .bashrc $HOME
# \cp -f anydesk.repo /etc/yum.repos.d/anydesk.repo

# 2. Move to $HOME
cd $HOME

# 3. remove the folder
# rm -rf rockylinux_devstd_installscript

# 4. change the install script mode to +x
chmod +x installscript.sh

# 5. excute the script with sourc
source installscript.sh
