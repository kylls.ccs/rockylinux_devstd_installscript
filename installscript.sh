#!/bin/bash

# global variable
USRNAME=$(whoami)


# updating the whole system
sudo dnf upgrade --refresh -y
sudo dnf update --refresh -y
sudo dnf install epel-release -y

# install git
# sudo dnf install git -y

# setting git
git config --global user.name "$USRNAME"
git config --global user.email "$(whoami)@dev.com"
# git clone https://gitlab.com/kylls.ccs/rockylinux_devstd_installscript.git

# cd install
# ./install

# install fcitx chewing
sudo dnf makecache --refresh
sudo dnf -y install fcitx-chewing

# install brave browser
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo dnf install brave-browser -y

# install htop -system monitor
sudo dnf install htop -y

# install nvm node version manager
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# install node v16.15.0
nvm install v16.15.0

# install vscode
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
printf "[vscode]\nname=packages.microsoft.com\nbaseurl=https://packages.microsoft.com/yumrepos/vscode/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscode.repo
sudo dnf install code -y

# install google-chrome
sudo dnf install wget -y
wget https://dl.google.com/linux/linux_signing_key.pub
sudo rpm --import linux_signing_key.pub
wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
sudo dnf install google-chrome-stable_current_x86_64.rpm -y
rm -rf google-chrome-stable_current_x86_64.rpm

# install python3 && python3-pip
sudo dnf install python3 -y

# install powerline
sudo pip3 install powerline-status
sudo pip3 install git+https://github.com/powerline/powerline.git@develop
git clone https://github.com/powerline/fonts.git
cd fonts
./install.sh
cd ..
rm -rf fonts

# install npm && typescript
npm install -g npm@latest
npm install -g typescript

#add anydesk repo
sudo tee /etc/yum.repos.d/anydesk.repo<<EOF
[anydesk]
name=AnyDesk CentOS - stable
baseurl=http://rpm.anydesk.com/centos/x86_64/
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://keys.anydesk.com/repos/RPM-GPG-KEY
EOF

# install anydesk
sudo dnf install -y anydesk

# install mono for C#
# sudo rpmkeys --import "http://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
# curl https://download.mono-project.com/repo/centos8-stable.repo | tee /etc/yum.repos.d/mono-centos8-stable.repo

# sudo yum install mono-devel

# install .NETCLI
sudo dnf install dotnet -y

# install rust-lag
cd $HOME
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
1

# install wps office
# wget https://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/11664/wps-office-11.1.0.11664.XA-1.x86_64.rpm
# sudo dnf install -y wps-office-11.1.0.11664.XA-1.x86_64.rpm

# remove wps install package
# rm -rf wps-office-11.1.0.11664.XA-1.x86_64.rpm

# remove install script
rm -rf installscript.sh

# create system update folder
mkdir system_update && cd system_update && git clone https://gitlab.com/kylls.ccs/rockylinux_devstd_installscript.git

cd $HOME




cd 桌面
rm -rf run.sh Install_Guide.txt

cd ..

# reboot