#!bin/bash

# create google fonts folder
sudo mkdir /usr/share/fonts/googlefonts 

# copy file to google fonts folder
sudo cp -f PTMono-Regular.ttf /usr/share/fonts/googlefonts

# cd google fonts folder
cd /usr/share/fonts/googlefonts

# excute
sudo fc-cache -fv
fc-match PTMono

cd $HOME



