#!/bin/bash

cd $HOME

# install sublime-text4
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo wget -P /etc/yum.repos.d/ https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
sudo dnf -y install sublime-text

# set the sublime-text4 setting
cd $HOME
git clone https://gitlab.com/kylls.ccs/sublimetextsetting.git
\cp -R sublimetextsetting/* ~/.config/sublime-text/
rm -rf sublimetextsetting

# back to HOME path
cd $HOME